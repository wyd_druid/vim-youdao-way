# -*- coding: utf-8 -*-
import urllib, re, collections, xml.etree.ElementTree as ET
import sys, json
import io, platform
import sqlite3

try:
    from urllib.parse import urlparse, urlencode
    from urllib.request import urlopen, Request
    from urllib.error import HTTPError
except ImportError:
    from urlparse import urlparse
    from urllib import urlencode
    from urllib2 import urlopen, Request, HTTPError
from playsound import playsound
import time
import pymysql

## connect to the database,create connect object and cursor object
def dbConnect():
    db = pymysql.connect(host="rm-2vcnbo598x3o8spp8wo.mysql.cn-chengdu.rds.aliyuncs.com",
                         port=3306,
                         user='wangyangdong',
                         password='PtMx5R63iEezgl6G',
                         db='word'
            )
    cursor = db.cursor()
    return db, cursor
## disconnect    
def dbDisconnect(db):
    db.close()

def insertData(word, explanation):
    word = preprocess_word(word)
    if not word:
        return ''
    word = word.lower()
    db, cursor = dbConnect()
    t = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) 
    url =  'http://dict.youdao.com' + '/dictvoice?audio=' +  url_quote(word)+'&type=1'
    sql = '''INSERT INTO word(word,result, update_at, num , flog, voice_url ) \
            VALUES ("%s", "%s", "%s", %d , %d, "%s") '''  % (word , explanation, t, 0 , 1 ,url)

    try:
         # 执行sql语句
        cursor.execute(sql)
        # 执行sql语句
        db.commit()
    except:
   # 发生错误时回滚
        db.rollback()
    # 关闭数据库连接
    db.close()

def str_encode(word):
    if sys.version_info >= (3, 0):
        return word
    else:
        return word.encode('utf-8')


def str_decode(word):
    if sys.version_info >= (3, 0):
        return word
    else:
        return word.decode('utf-8')


def bytes_decode(word):
    if sys.version_info >= (3, 0):
        return word.decode()
    else:
        return word


def url_quote(word):
    if sys.version_info >= (3, 0):
        return urllib.parse.quote(word)
    else:
        return urllib.quote(word.encode('utf-8'))


WARN_NOT_FIND = " 找不到该单词的释义"
ERROR_QUERY = " 有道翻译查询出错!"
NETWORK_ERROR = " 无法连接有道服务器!"

QUERY_BLACK_LIST = ['.', '|', '^', '$', '\\', '[', ']', '{', '}', '*', '+', '?', '(', ')', '&', '=', '\"', '\'', '\t']


def preprocess_word(word):
    word = word.strip()
    for i in QUERY_BLACK_LIST:
        word = word.replace(i, ' ')
    array = word.split('_')
    word = []
    p = re.compile('[a-z][A-Z]')
    for piece in array:
        lastIndex = 0
        for i in p.finditer(piece):
            word.append(piece[lastIndex:i.start() + 1])
            lastIndex = i.start() + 1
        word.append(piece[lastIndex:])
    return ' '.join(word).strip()

def play_word_sound(word):
    word = preprocess_word(word)
    if not word:
        return ''
    try:
        r = urlopen('http://dict.youdao.com' + '/dictvoice?audio=' +
                url_quote(word)+'&type=1')
    except IOError:
        return 
    if r.getcode() == 200:
        my_open = open("/tmp/tmp.mp3", 'wb+')
        my_open.write( r.read())
        my_open.close()
        playsound("/tmp/tmp.mp3")
    return

def get_word_info(word):
    word = preprocess_word(word)
    if not word:
        return ''
    try:
        r = urlopen('http://dict.youdao.com' + '/fsearch?q=' + url_quote(word))
    except IOError:
        return NETWORK_ERROR
    if r.getcode() == 200:
        doc = ET.fromstring(r.read())

        phrase = doc.find(".//return-phrase").text
        p = re.compile(r"^%s$" % word, re.IGNORECASE)
        if p.match(phrase):
            info = collections.defaultdict(list)

            if not len(doc.findall(".//content")):
                return WARN_NOT_FIND

            for el in doc.findall(".//"):
                if el.tag in ('return-phrase', 'phonetic-symbol'):
                    if el.text:
                        info[el.tag].append(el.text.encode("utf-8"))
                elif el.tag in ('content', 'value'):
                    if el.text:
                        info[el.tag].append(el.text.encode("utf-8"))

            for k, v in info.items():
                info[k] = b' | '.join(v) if k == "content" else b' '.join(v)
                info[k] = bytes_decode(info[k])

            tpl = ' %(return-phrase)s'
            if info["phonetic-symbol"]:
                tpl = tpl + ' [%(phonetic-symbol)s]'
            tpl = tpl + ' %(content)s'

            return tpl % info

        else:
            try:
                r = urlopen("http://fanyi.youdao.com" + "/translate?i=" + url_quote(word), timeout=5)
            except IOError:
                return NETWORK_ERROR

            p = re.compile(r"global.translatedJson = (?P<result>.*);")

            r_result = bytes_decode(r.read())
            s = p.search(r_result)
            if s:
                r_result = json.loads(s.group('result'))
                if r_result is None:
                    return str_decode(s.group('result'))

                error_code = r_result.get("errorCode")
                if error_code is None or error_code != 0:
                    return str_decode(s.group('result'))

                translate_result = r_result.get("translateResult")
                if translate_result is None:
                    return str_decode(s.group('result'))

                translate_result_tgt = ''
                for i in translate_result:
                    translate_result_tgt = translate_result_tgt + i[0].get("tgt") + "\n"

                return translate_result_tgt
            else:
                return ERROR_QUERY
    else:
        return ERROR_QUERY





if __name__ == "__main__":
    if(platform.system()=='Windows'):
        sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='utf8')
    argv = sys.argv

    if(argv[1] == "1"):
        play_word_sound(str_decode("".join(argv[2:])))
    else: 
        if(argv[1]== "0"):
            info = get_word_info(str_decode("".join(argv[2:])))
            sys.stdout.write(info)
        else: 
            info = get_word_info(str_decode("".join(argv[2:])))
            if ( info != ERROR_QUERY and info != NETWORK_ERROR and info != WARN_NOT_FIND ):
                insertData(str_decode("".join(argv[2:])),info)


